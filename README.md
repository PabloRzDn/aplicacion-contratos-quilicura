# Aplicación para la gestión de contratos en Municipalidad de Quilicura

## El presente repositorio contiene los archivos y módulos correspondientes al desarrollo de aplicación web para la gestión de los contratos.
---
## Índice
- #### Resumen
- #### Funcionalidades y capturas
- #### Instalación
---

### Resumen

Actualmente, la gestión y seguimiento de los distintos tipos de contratos en la Municipalidad de Quilicura corresponde a un proceso análogo, a mano y descentralizado. En este sentido, no existe la unificación de la información, hay desconocimiento sobre el estado actual de los contratos, y tampoco hay certeza en tiempo real sobre las cláusulas, obligaciones y derechos. Esto tiene como consecuencia que, tanto en el área jurídica como de administración no existan certezas sobre el universo de los contratos, su vigencia, la situación contractual, observaciones y garantías. La sistematización actual se basa en una serie de documentos físicos y digitales desagregados en las diversas áreas de competencia dentro de la municipalidad.

La aplicación contenida en este repositorio pretende contribuir a la digitalización de las actividades realizadas en la Municipalidad de Quilicura, en específico, la gestión y administración de los contratos. Esto, a través de una web capaz de reproducir la mayor cantidad de datos contenidos en la linea de tiempo de un contrato.


### Funcionalidades y Capturas

Respecto a los usuarios y usuarias de este sistema, la aplicación sigue los siguientes preceptos:

#### General

___
La aplicación cuenta con acceso restringido y encriptación de contraseña.

![acceso_aplicacion](/SCREENSHOT/app-contratos-ingreso.png )
___
![login_aplicacion](/SCREENSHOT/app-contratos-login.png)
---

#### Administradores

- La función del administrador/a es realizar el seguimiento y mantenimiento de los contratos. Es decir, puede crearlos, asignarlos a un usuario con menores privilegios (Usuario ITS), revisar facturas, multas, y documentación asociada.
- Puede crear y editar nuevos Administradores y usuarios ITS.
- Tiene acceso a métricas de los contratos.
- Tiene acceso a la documentación de los contratos.

---
![dashboard_admin-1](SCREENSHOT/app-contratos-dashboard-admin.png)

---
![dashboard_admin-2](SCREENSHOT/app-contratos-dashboard-admin2.png)

---
![gestion_admin](SCREENSHOT/app-contratos-gestion-admin.png)

---
![registro_contratos](SCREENSHOT/app-contratos-registro-contrato.png)


#### Usuarios ITS


- Los usuarios ITS son quienes documentan el contrato, en relación a las multas y facturas.
- Tienen acceso al seguimiento del contrato y a la documentación.
- Pueden cambiar el status de las facturas.

---

![dashboard_user](SCREENSHOT/app-contratos-dashboard-user.png)


---

![dashboard_user](SCREENSHOT/app-contratos-dashboard-user2.png)

---

![facturas](SCREENSHOT/app-contratos-facturas.png)

---

#### Instalación

La aplicación está desarrollada en el Framework Flask de Python, ocupando Sqlite3 (para esta version) y el ORM Sqlalchemy. Todos los requerimientos se pueden instalar con el gestor pip, a través del archivo requirements.txt. Para evitar dificultades propios de cada máquina (sistemas operativos, versiones de Python, etc.) la aplicación se monta en un contenedor Docker (para conocer más sobre contenedores docker, visitar https://docs.docker.com/)


###### Requisitos
- Git instalado (git bash)
- Docker instalado (https://docs.docker.com/get-docker/)

- (En windows) Windows Subsystem for Linux (WSL2)

###### Crear entorno virtual

En primer lugar, entrar al bash de git y crear un entorno virtual en el directorio de la aplicación 


~~~ 
$ virtualenv venv
~~~

Luego, activar el entorno virtual.

**Windows**:
~~~
$ source venv/Scripts/activate
~~~
**Linux (Debian, Ubuntu, etc.)**:
~~~
$ source venv/bin/activate
~~~



###### Montar Imagen

Una vez se ha instalado Docker, se debe construir la imagen, según el archivo Dockerfile contenido en el repositorio. Para esto, situarse en el mismo directorio e introducir:

~~~
$ docker build -t contratos-app .
~~~

###### Correr Imagen

Confirmado el montaje de la imagen, correr la aplicación en el puerto a elección (en este caso, puerto 6000)

~~~
$ docker run -p 6000:5000 contratos-app
~~~


###### Entrar a la aplicación

Para entrar a la aplicación, se debe introducir la IP del servidor o máquina. Localmente, el acceso se encuentra en:

~~~
http://localhost:5000
~~~