# Este Script define las configuraciones específicas para los distintos ambientes.
import os


class Config(object):
     DEBUG=False
     TESTING=False
     
     SECRET_KEY="secret_key"
     #SQLALCHEMY_DATABASE_URI="mysql+pymysql://root@localhost/contratos_db"
     SQLALCHEMY_DATABASE_URI="sqlite:///contratos_db.db"
     SQLALCHEMY_TRACK_MODIFICATIONS=True
     APP_ROOT = os.path.dirname(os.path.abspath(__file__))
     UPLOAD_FOLDER = os.path.join(APP_ROOT, 'files')
     
   
    

class ProductionConfig(Config):
    DB_NAME="production-db"
    DB_USERNAME="production"
   

class DevelopmentConfig(Config):
    DEBUG=True
    #SQLALCHEMY_DATABASE_URI="mysql:pymysql//root@localhost/contratos_db"
    #SQLALCHEMY_TRACK_MODIFICATIONS=False
    
    
   
   
class TestingConfig(Config):
    TESTING=True


