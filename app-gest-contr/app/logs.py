from datetime import date



class LogsTareas():
    def __init__(self,usuario_actual,fechahora_log,mod):
        self.usuario_actual=usuario_actual
        self.fechahora_log=fechahora_log
        self.mod=mod

    
    def logCrearUsuario(self):
        self.Log="El/la Administrador/a {} ha creado el contacto {}, el día y hora {}".format(self.usuario_actual,self.mod,self.fechahora_log) 
        return self.Log

    def logContrato(self):
        self.Log="El/la Usuario/a {} ha creado un Contrato el día y hora {}".format(self.usuario_actual,self.fechahora_log)
        return self.Log 

    def logeliminarUno(self):
        self.Log="El/la Usuario/a {} ha sido eliminado por {} el día y hora {}".format(self.mod,self.usuario_actual,self.fechahora_log)
        return self.Log

    def logEditarUsuario(self):
        self.Log="El/la Usuario/a {} ha sido editado por {} el día y hora {}".format(self.mod,self.usuario_actual,self.fechahora_log)
        return self.Log

    def logRegistrarMulta(self):
        self.Log="El/la Usuario/a {} ha creado una multa el día y hora {}".format(self.usuario_actual,self.fechahora_log)
        return self.Log

    def logRegistrarFactura(self):
        self.Log="El/la Usuario/a {} ha creado la factura {} el día y hora {}".format(self.usuario_actual,self.mod,self.fechahora_log)
        return self.Log

    def cerrarContrato(self):
        self.Log="El/la Usuario/a {} ha cerrado el contrato {} el día y hora {}".format(self.usuario_actual,self.mod,self.fechahora_log)


    def logEditarFactura(self):
        self.Log="El/la Usuario/a {} ha editado la factura {} el día y hora {}".format(self.usuario_actual,self.mod,self.fechahora_log)
        return self.Log