import os, re
from app import app, db
from werkzeug.utils import secure_filename
from flask import request, flash, redirect, url_for
import datetime
from .actiondb import ModeloBaseDB
from app.auth.models import Archivo


class Archivos:
    def __init__(self):
        pass

        #self.archivo=archivo
        
    
    def archivo_permitido(self,nombre_archivo):
        return "." in nombre_archivo and nombre_archivo.rsplit(".",1)[1].lower() in {"pdf"}

    def subir_archivo(self):

        nomenclatura=[
            "OE", #Oferta Económica
            "OT", #Oferta Técnica
            "BA", #Bases Adminstrativas
            "BT", #Bases técnicas
            "CON", #Consultas
            "GAR", #Garantías
            "IG" #Información General
        ]
        lista_archivos=[]

        for tipoarchivo in nomenclatura:

        
            if tipoarchivo not in request.files:
                flash("El archivo no se encuentra en el campo de subida.")
                return redirect(url_for("admin.TestUpload"))
            
            self.archivo=request.files[tipoarchivo]
            self.rut_contratante=request.form["rut_contratante"]

            self.nuevo_nombre="{}_{}_{}.pdf".format(
                datetime.datetime.today().strftime("%y%m%d"),
                tipoarchivo,
                self.rut_contratante

            )

            self.archivo.filename=self.nuevo_nombre

            lista_archivos.append(self.archivo.filename)
            
            if self.archivo.filename=="":
                flash("El campo está vacío")
                return redirect(url_for("admin.TestUpload"))

            if self.archivo and self.archivo_permitido(self.archivo.filename):

               
               
                
                self.nombre_archivo=secure_filename(self.archivo.filename)
                self.archivo.save(os.path.join(app.config["UPLOAD_FOLDER"],self.nombre_archivo))
        

        archivos=Archivo(
            lista_archivos[0],
            lista_archivos[1],
            lista_archivos[2],
            lista_archivos[3],
            lista_archivos[4],
            lista_archivos[5],
            lista_archivos[6]
        )

        ModeloBaseDB.guardar(archivos)
        
        


        flash("Archivo subido correctamente")
        
        return redirect(url_for("admin.Dashboard"))        
    
    

    def seleccionar_orden(self,cls,ord,form):
        
        self.for_regex=form
        
        if re.search("OE",ord):
            self.col=db.session.query(cls.ofertaEconomica).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data
        elif re.search("OT",ord):
            self.col=db.session.query(cls.ofertaTecnica).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data
        elif re.search("BA",ord):
            self.col=db.session.query(cls.basesAdministrativas).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data
        elif re.search("BT",ord):
            self.col=db.session.query(cls.basesTecnicas).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data
        elif re.search("CON",ord):
            self.col=db.session.query(cls.consultas).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data

        elif re.search("GAR",ord):
            self.col=db.session.query(cls.garantias).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data
        elif re.search("IG",ord):
            self.col=db.session.query(cls.informacionGeneral).all()
            self.data=self.iterar_regex(self.col,self.for_regex)
            return self.data
        else:
            return "False"

    @classmethod        
    def iterar_regex(self,col,for_regex):
        for dato in enumerate(col):
            self.dato_db=col[dato[0]][0]
            busq_regex=re.search(for_regex,self.dato_db)

            if busq_regex is not None:
                return self.dato_db
        
        return "No hay nara"