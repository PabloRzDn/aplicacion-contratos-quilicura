
#importación de librerias externas
import os, sys
from flask import Blueprint, render_template, request, redirect, url_for, send_file, send_from_directory
import datetime
from flask.helpers import flash
from pygal import style
from sqlalchemy.sql import func
from sqlalchemy import exc
from sqlalchemy.sql.elements import Null
from flask_login import login_required, current_user
from werkzeug.security import generate_password_hash
from werkzeug.utils import secure_filename
import re

#importación librerias internas
from app import db, ma, app
from app.auth.models import Facturas, Multas, User, Contrato, Logs, Archivo
from app.auth.forms import RegistroUser
from app.admin.forms import RegistroContrato
from app.logs import LogsTareas
from app.actiondb import ModeloBaseDB
from app.upload import Archivos
from app.indicadores import Indicadores,GraficoTorta
from app.user.views import Multa

admin=Blueprint("admin",__name__,template_folder="templates")

#funión para subir archivos
def allowed_file(filename):
    return "." in filename and filename.rsplit(".",1)[1].lower() in {"pdf"}

@admin.route("/")
@login_required
def Index():
    return render_template("index.html")

@admin.route("/dashboard")
@login_required
def Dashboard():
   
    contador_mas_de_120=0
    contador_mas_de_90=0
    contador_mas_de_30=0
    contador_menos_de_30=0
    contador_vencidos=0
    

    tabla_logs=ModeloBaseDB.seleccionarTodoDesc(Logs)
    
    indicadores_facturas=Indicadores.obtener_contadores_facturas(Facturas)
    leyenda_facturas=["Registradas","Aceptadas","Rechazadas"]
    grafico_facturas=GraficoTorta(leyenda_facturas,indicadores_facturas)
    grafico_facturas=grafico_facturas.generar_grafico()
    print("[+] Grafico de facturas generado",file=sys.stderr)
    indicadores_multas=Indicadores.obtener_contadores_multas(Multas)
    leyenda_multas=["Cursadas","Notificadas","Impugnadas","Apeladas","ejecutoriadas"]
    grafico_multas=GraficoTorta(leyenda_multas,indicadores_multas)
    grafico_multas=grafico_multas.generar_grafico()
    print("[+] Grafico de multas cargado",file=sys.stderr)

   
    fechas_contratos=ModeloBaseDB.seleccionarColumnaFechas(Contrato)
    for i in range(fechas_contratos.count()):
        plazo_vencimiento=Indicadores(str(fechas_contratos.all()[i][0]),datetime.datetime.now())
        plazo_vencimiento=int(plazo_vencimiento.vencimiento_contratos())
        print(plazo_vencimiento)
        
     
        if plazo_vencimiento<0:
            contador_vencidos+=1
        elif plazo_vencimiento > 120:
            contador_mas_de_120+=1
        elif plazo_vencimiento<=120 and plazo_vencimiento>90:
            contador_mas_de_90+=1
        elif plazo_vencimiento<=90 and plazo_vencimiento>30:
            contador_mas_de_30+=1
        elif plazo_vencimiento<=30:
            contador_menos_de_30+=1

    indicadores_contratos=[contador_mas_de_120,contador_mas_de_90,contador_mas_de_30,contador_menos_de_30,contador_vencidos]
    leyenda=["Más de 120 días","Más de 90 días", "Más de 30 días", "Menos de 30 días", "Vencidos"]
    
    data_grafico=GraficoTorta(leyenda,indicadores_contratos)
    data_grafico=data_grafico.generar_grafico()
    print("[+] Grafico de contratos cargado",file=sys.stderr)


    contratos_totales=ModeloBaseDB.contarCol(Contrato)
    return render_template("dashboard.html",
    estado_facturas=indicadores_facturas, 
    contratos_totales=contratos_totales,
    indicadores_contratos=indicadores_contratos,
    data_grafico=data_grafico,
    grafico_facturas=grafico_facturas,
    grafico_multas=grafico_multas,
    tabla_logs=tabla_logs
    )

@admin.route("/crearcontrato")
@login_required
def FormularioContrato():

    usuarios_its=User.query.filter_by(rolUser="USUARIO")
    
    return render_template("crearcontrato.html", usuarios_its=usuarios_its)

@admin.route("/formulariocontrato", methods=["GET","POST"])
@login_required
def CrearContrato():
    formulario_contrato=RegistroContrato(request.form)
    if request.method=="POST":
        print("[+] método POST",file=sys.stderr)


        #si el method es Post, obtiene los datos del contrato según clase de forms
        contrato=Contrato(
            formulario_contrato.its_id.data,
            formulario_contrato.tituloContrato.data,
            formulario_contrato.labores.data,
            formulario_contrato.idContrato.data,
            formulario_contrato.montoRemuneracion.data,
            formulario_contrato.emailFirmante.data,
            formulario_contrato.periodoPago.data,
            formulario_contrato.plazoContrato.data,
            formulario_contrato.duracionJornada.data,
            formulario_contrato.distribucionJornada.data,

            formulario_contrato.razonSocial.data,
            formulario_contrato.rutRazonSocial.data,
            formulario_contrato.nombreContratante.data,
            formulario_contrato.rutContratante.data,
            formulario_contrato.nacionalidadContratante.data,
            formulario_contrato.fechaInicioContrato.data,
            formulario_contrato.domicilioRazonSocial.data,
            formulario_contrato.fechaFinContrato.data,
            str(datetime.datetime.now())
        )

        usuario_log="{} {}".format(current_user.nombresUser,current_user.apellidosUser)
        tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S"))) 

        log_contrato=LogsTareas(usuario_log, tiempo_creacion,None)
        
      
        log_contrato=log_contrato.logContrato()
        
        log=Logs(
            current_user.id,
            tiempo_creacion,
            log_contrato
        )
        ModeloBaseDB.guardar(contrato)
        print("[+] contrato guardado",file=sys.stderr)

        ModeloBaseDB.guardar(log)
        print("[+] log guardado",file=sys.stderr)

        flash(str("Contrato "+formulario_contrato.tituloContrato.data+" Creado satisfactoriamente"))
        
        return render_template("documentacion.html",rut_contratante=formulario_contrato.rutContratante.data)



@admin.route("/upload", methods=["GET","POST"])
@login_required
def Upload():

    if request.method=="POST":
        archivo=Archivos() 
        archivo.subir_archivo()
        print("[+] Archivos subidos",file=sys.stderr)

        
    
   

    return redirect(url_for("admin.Dashboard"))



@admin.route("/download/<string:orden_rutContratante>", methods=["GET","POST"])
def Download(orden_rutContratante):
    try:
        orden_regex="^{}".format(orden_rutContratante)
        formato_regex="{}.pdf$".format(orden_rutContratante)
    
        archivo_seleccionado=Archivos.seleccionar_orden(Archivos,Archivo,orden_regex,formato_regex)
        print("[+] Archivo encontrado",file=sys.stderr)

        return send_from_directory(directory=app.config["UPLOAD_FOLDER"],path=archivo_seleccionado)

    except:
        print("[!] Archivo no existe",file=sys.stderr)

        flash("El archivo no ha sido encontrado")
        if current_user.rolUser=="ADMINISTRADOR":
            return redirect(url_for("admin.SeguirContrato"))
        elif current_user.rolUser=="USUARIO":
            return redirect(url_for("user.Dashboard"))


@admin.route("/modificarperfiles", methods=["GET","POST"])
@login_required
def ModificarPerfiles():
    if current_user.rolUser=="ADMINISTRADOR":
        total_usuarios=ModeloBaseDB.seleccionarTodo(User)
        

        return render_template("modificarperfiles.html", total_usuarios=total_usuarios)

@admin.route("/eliminarperfil/<string:id>", methods=["GET","POST"])
@login_required
def EliminarPerfil(id):

    its_log="{} {}".format(current_user.nombresUser,current_user.apellidosUser)
    user_a_eliminar="{} {}".format(
        ModeloBaseDB.seleccionarId(User,id).nombresUser,
        ModeloBaseDB.seleccionarId(User,id).apellidosUser)

    tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S"))) 

    log_eliminar=LogsTareas(its_log, 
    tiempo_creacion,
    user_a_eliminar)

    log_eliminar=log_eliminar.logeliminarUno()
    log=Logs(current_user.id,
    tiempo_creacion,
    log_eliminar)

    ModeloBaseDB.guardar(log)
    print("[+] Log almacenado",file=sys.stderr)

    ModeloBaseDB.eliminarUno(User,id)
    print("[+] Usuario Eliminado",file=sys.stderr)
    flash(str("Usuario "+user_a_eliminar+" ha sido eliminado Satisfactoriamente"))
    return redirect(url_for("admin.ModificarPerfiles"))


@admin.route("/editarperfil/<id>", methods=["GET","POST"])
@login_required
def EditarPerfil(id):
    user_a_editar=ModeloBaseDB.seleccionarId(User,id)
    return render_template("editarperfil.html", user_a_editar=user_a_editar)


@admin.route("/actualizarperfil/<id>", methods=["GET","POST"])
@login_required
def ActualizarPerfil(id):
    formulario_edicion=RegistroUser(request.form)
    if request.method=="POST":
        user_a_editar="{} {}".format(
        ModeloBaseDB.seleccionarId(User,id).nombresUser,
        ModeloBaseDB.seleccionarId(User,id).apellidosUser)

        its_log="{} {}".format(current_user.nombresUser,current_user.apellidosUser)
        tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S")))
        ModeloBaseDB.editarUsuario(User,id,formulario_edicion)

        log_editar=LogsTareas(its_log,tiempo_creacion,user_a_editar)
        log_editar=log_editar.logEditarUsuario()
        log=Logs(current_user.id,
            tiempo_creacion,
            log_editar)

        ModeloBaseDB.guardar(log)

        flash(str("Usuario "+formulario_edicion.nombresUser.data+ " editado exitosamente."))
        return redirect(url_for("admin.ModificarPerfiles"))



@admin.route("/seguircontrato", methods=["GET","POST"])
@login_required
def SeguirContrato():
    total_contratos=ModeloBaseDB.seleccionarTodo(Contrato)

    return render_template("seguircontratos.html", total_contratos=total_contratos)


@admin.route("/contrato/<id>", methods=["GET","POST"])
@login_required
def ModeloContrato(id):
    modelo_contrato=ModeloBaseDB.seleccionarId(Contrato,id)
    facturas=ModeloBaseDB.seleccionarPorContratoId(Facturas,id)
    multas=ModeloBaseDB.seleccionarPorContratoId(Multas,id)
    id_its=ModeloBaseDB.seleccionarId(Contrato,id).its_id
    its=ModeloBaseDB.seleccionarId(User,id_its)


    return render_template("contrato.html",
    its=its,
    modelo_contrato=modelo_contrato,
    facturas=facturas,
    multas=multas)


@admin.route("/cerrarcontrato/<string:id>", methods=["GET","POST"] )
def CerrarContrato(id):
    user="{} {}".format(current_user.nombresUser,current_user.apellidosUser)
    contrato=ModeloBaseDB.seleccionarId(Contrato,id)
    contrato=contrato.tituloContrato
    ModeloBaseDB.eliminarUno(Contrato,id)
    log_cerrar_contrato=LogsTareas(user,datetime.datetime.now(),contrato)
    log_cerrar_contrato.cerrarContrato()
    log=Logs(current_user.id,str(datetime.datetime.now()),log_cerrar_contrato)



    flash("Contrato cerrado Satisfactoriamente")
    return redirect(url_for("admin.SeguirContrato"))


@admin.route("/sanciones", methods=["GET","POST"])
def Sanciones():
    total_multas=ModeloBaseDB.seleccionarTodo(Multas)
    return render_template("sanciones.html", total_multas=total_multas)



@admin.route("/construccionadmin")
def ConstruccionAdmin():

    return render_template("construccion.html")