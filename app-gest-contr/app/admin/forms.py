from sqlalchemy.sql.sqltypes import String
from flask_wtf import FlaskForm
from wtforms import Form, SelectField, SubmitField, DateField,StringField, PasswordField, DateTimeField, validators
from wtforms.validators import DataRequired

#Formulario para generar contratos y contratantes

class RegistroContrato(Form):
    its_id=SelectField("its_id",validators=[DataRequired()])
    tituloContrato=StringField("tituloContrato", validators=[DataRequired()])
    labores=StringField("labores", validators=[DataRequired()])
    idContrato=StringField("idContrato", validators=[DataRequired()])   
    montoRemuneracion=StringField("montoRemuneracion", validators=[DataRequired()])
    emailFirmante=StringField("emailFirmante", validators=[DataRequired()])
    periodoPago=StringField("periodoPago", validators=[DataRequired()])
    plazoContrato=StringField("plazoContrato", validators=[DataRequired()])
    duracionJornada=StringField("duracionJornada", validators=[DataRequired()])
    distribucionJornada=StringField("distribucionJornada",validators=[DataRequired()])
    
    razonSocial=StringField("razonSocial",validators=[DataRequired()])
    rutRazonSocial=StringField("rutRazonSocial", validators=[DataRequired()])
    nombreContratante=StringField("nombreContratante",validators=[DataRequired()])
    rutContratante=StringField("rutContratante", validators=[DataRequired()])
    nacionalidadContratante=StringField("nacionalidadContratante",validators=[DataRequired()])
    fechaInicioContrato=DateField("fechaInicioContrato",validators=[DataRequired()])
    domicilioRazonSocial=StringField("domicilioRazonSocial", validators=[DataRequired()])
    fechaFinContrato=DateField("fechaFinContrato",validators=[DataRequired()])
    
   

    

   
   
   
    