from app import app
from flask import render_template



@app.route("/")
def Index():
    return render_template("index.html")

@app.route("/construccion")
def Construccion():
    return render_template("construccion.html")

