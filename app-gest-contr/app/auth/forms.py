from flask_wtf import FlaskForm, Form
from wtforms import SubmitField, DateField,StringField, SelectField, PasswordField, DateTimeField, validators
from wtforms.validators import DataRequired, InputRequired, Email, Length

class RegistroUser(Form):
    
    nombresUser=StringField("nombresUser", validators=[DataRequired()])
    apellidosUser=StringField("apellidosUser", validators=[DataRequired()])
    rutUser=StringField("rutUser", validators=[DataRequired()])
    fechaNacimientoUser=DateField("fechaNacimientoUser", validators=[DataRequired()])
    emailUser=StringField("emailUser", validators=[DataRequired()])
    telefonoUser=StringField("telefonoUser", validators=[DataRequired()])
    cargoUser=StringField("cargoUser", validators=[DataRequired()])
    contrasenaUser=PasswordField("contrasenaUser", validators=[DataRequired()])
    rolUser=StringField("rolUser", validators=[DataRequired()])


class Login(FlaskForm):
    emailUser=StringField("emailUser", validators=[DataRequired()])
    contrasenaUser=StringField("contrasenaUser", validators=[DataRequired()])

