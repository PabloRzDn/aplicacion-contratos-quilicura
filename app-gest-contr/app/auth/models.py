from sqlalchemy.sql import func
from sqlalchemy import DateTime,Integer, ForeignKey, Column
from sqlalchemy.orm import base, relation, relationship
from sqlalchemy.sql.expression import null
from sqlalchemy.sql.sqltypes import Date, String
from app import db, ma
from flask_login import UserMixin



class User(UserMixin,db.Model):
    __tablename__="user"
    id=Column(Integer, primary_key=True)
    nombresUser=db.Column(db.String(255), unique=False, index=False, nullable=False)
    apellidosUser=db.Column(db.String(255), unique=False, index=False, nullable=False)
    rutUser=db.Column(db.String(10), unique=True, index=False, nullable=False)
    fechaNacimientoUser=db.Column(db.Date(), unique=False, index=False, nullable=False)
    emailUser=db.Column(db.String(255), unique=True, index=False, nullable=False)
    telefonoUser=db.Column(db.String(12), unique=False, index=False, nullable=False)
    cargoUser=db.Column(db.String(255), unique=False, index=False, nullable=False)
    contrasenaUser=db.Column(db.String(255), unique=False, index=False, nullable=False)
    rolUser=db.Column(db.String(255),unique=False, index=False, nullable=False)
    fechaMod=db.Column(db.String(256), unique=True)
    
    contrato_rel=relationship("Contrato",primaryjoin="User.id==Contrato.its_id")
    logs_rel=relationship("Logs", primaryjoin="User.id==Logs.its_id")
    multas_rel=relationship("Multas",primaryjoin="User.id==Multas.user_id")
    facturas_rel=relationship("Facturas",primaryjoin="User.id==Facturas.user_id")

    def __init__(self,
    nombresUser,
    apellidosUser,
    rutUser,
    fechaNacimientoUser,
    emailUser,
    telefonoUser,
    cargoUser,
    contrasenaUser,
    rolUser,
    fechaMod):
   
    
        self.nombresUser=nombresUser
        self.apellidosUser=apellidosUser
        self.rutUser=rutUser
        self.fechaNacimientoUser=fechaNacimientoUser
        self.emailUser=emailUser
        self.telefonoUser=telefonoUser
        self.cargoUser=cargoUser
        self.contrasenaUser=contrasenaUser
        self.rolUser=rolUser
        self.fechaMod=fechaMod

    def get_rol(self):
        return self.rolUser
db.create_all()


class Contrato(UserMixin,db.Model):
    __tablename__="contratos"
    id=db.Column(db.Integer, primary_key=True)
    its_id=Column(Integer, ForeignKey("user.id"))
    tituloContrato=db.Column(db.String(255),unique=False, index=False, nullable=False)
    labores=db.Column(db.String(255), unique=False, index=False, nullable=False)
    idContrato=db.Column(db.String(255), unique=False, index=False, nullable=False)
    montoRemuneracion=db.Column(db.String(255), unique=False, index=False, nullable=False)
    emailFirmante=db.Column(db.String(255), unique=False, index=False, nullable=False)
    periodoPago=db.Column(db.String(255), unique=False, index=False, nullable=False)
    plazoContrato=db.Column(db.String(255), unique=False, index=False, nullable=False)
    duracionJornada=db.Column(db.String(255), unique=False, index=False, nullable=False)
    distribucionJornada=db.Column(db.String(255), unique=False, index=False, nullable=False)

    razonSocial=db.Column(db.String(255),unique=False, index=False, nullable=False)
    rutRazonSocial=db.Column(db.String(10),unique=False, index=False, nullable=False)
    nombreContratante=db.Column(db.String(255),unique=False, index=False, nullable=False)
    rutContratante=db.Column(db.String(10), unique=False, index=False, nullable=False)
    nacionalidadContratante=db.Column(db.String(255), unique=False, index=False, nullable=False)
    fechaInicioContrato=db.Column(db.Date(), unique=False, index=False, nullable=False)
    domicilioRazonSocial=db.Column(db.String(255), unique=False, index=False, nullable=False)
    fechaFinContrato=db.Column(db.Date(), unique=False, index=False, nullable=False)

    fechaMod=db.Column(db.String(256), unique=True)
    
    multas_rel=relationship("Multas",primaryjoin="Contrato.id==Multas.contrato_id")
    facturas_rel=relationship("Facturas",primaryjoin="Contrato.id==Facturas.contrato_id")
    

    def __init__(self,
    its_id,
    tituloContrato,
    labores,
    idContrato,
    montoRemuneracion,
    emailFirmante,
    periodoPago,
    plazoContrato,
    duracionJornada,
    distribucionJornada,

    razonSocial,
    rutRazonSocial,
    nombreContratante,
    rutContratante,
    nacionalidadContratante,
    fechaInicioContrato,
    domicilioRazonSocial,
    fechaFinContrato,
    fechaMod):
        self.its_id=its_id
        self.tituloContrato=tituloContrato
        self.labores=labores
        self.idContrato=idContrato
        self.montoRemuneracion=montoRemuneracion
        self.emailFirmante=emailFirmante
        self.periodoPago=periodoPago
        self.plazoContrato=plazoContrato
        self.duracionJornada=duracionJornada
        self.distribucionJornada=distribucionJornada

        self.razonSocial=razonSocial
        self.rutRazonSocial=rutRazonSocial
        self.nombreContratante=nombreContratante
        self.rutContratante=rutContratante
        self.nacionalidadContratante=nacionalidadContratante
        self.fechaInicioContrato=fechaInicioContrato
        self.domicilioRazonSocial=domicilioRazonSocial
        self.fechaFinContrato=fechaFinContrato
        self.fechaMod=fechaMod

db.create_all()       


class Archivo(UserMixin,db.Model):
    __tablename__="archivos"
    id=Column(Integer,primary_key=True)
    ofertaEconomica=Column(String(255),unique=False, index=False, nullable=True)
    ofertaTecnica=Column(String(255),unique=False, index=False, nullable=True)
    basesAdministrativas=Column(String(255),unique=False, index=False, nullable=True)
    basesTecnicas=Column(String(255),unique=False, index=False, nullable=True)
    consultas=Column(String(255),unique=False, index=False, nullable=True)
    garantias=Column(String(255),unique=False, index=False, nullable=True)
    informacionGeneral=Column(String(255),unique=False, index=False, nullable=True)

    def __init__(self,
                ofertaEconomica,
                ofertaTecnica,
                basesAdministrativas,
                basesTecnicas,
                consultas,
                garantias,
                informacionGeneral
                ):
        self.ofertaEconomica=ofertaEconomica
        self.ofertaTecnica=ofertaTecnica
        self.basesAdministrativas=basesAdministrativas
        self.basesTecnicas=basesTecnicas
        self.consultas=consultas
        self.garantias=garantias
        self.informacionGeneral=informacionGeneral
db.create_all()
db.session.commit()

class Logs(UserMixin,db.Model):
    __tablename__="logs"

    id=Column(Integer,primary_key=True)
    its_id=Column(Integer, ForeignKey("user.id"))
    logUser=Column(db.String(255),unique=False, index=False, nullable=False)
    fechaMod=db.Column(db.String(256), unique=True)
   
    def __init__(self,
    its_id,
    fechaMod,
    logUser):

        self.its_id=its_id
        self.logUser=logUser
        self.fechaMod=fechaMod
db.create_all()
db.session.commit()    

class Multas(UserMixin,db.Model):
    __tablename__="multas"
    id=Column(Integer,primary_key=True)
    user_id=Column(Integer,ForeignKey("user.id"))
    contrato_id=Column(Integer,ForeignKey("contratos.id"))
    glosa=db.Column(db.String(255),unique=False, index=False, nullable=False)
    monto=db.Column(db.String(255), unique=False, index=False, nullable=False)
    statusMulta=db.Column(db.String(255),unique=False, index=False, nullable=False)

    def __init__(self,
    user_id,
    contrato_id,
    glosa,
    monto,
    statusMulta):
        self.user_id=user_id
        self.contrato_id=contrato_id
        self.glosa=glosa
        self.monto=monto
        self.statusMulta=statusMulta
db.create_all()
db.session.commit()


class Facturas(UserMixin,db.Model):
    __tablename__="facturas"

    id=Column(Integer,primary_key=True)
    user_id=Column(Integer,ForeignKey("user.id"))
    contrato_id=Column(Integer,ForeignKey("contratos.id"))
    docFactura=Column(db.String(255), unique=False, index=False, nullable=False)
    statusFactura=Column(db.String(50), unique=False, index=False, nullable=False)
    fechaMod=Column(db.String(255), unique=False, index=False, nullable=False)

    def __init__(self,
    user_id,
    contrato_id,
    docFactura,
    statusFactura,
    fechaMod):
        self.user_id=user_id
        self.contrato_id=contrato_id
        self.docFactura=docFactura
        self.statusFactura=statusFactura
        self.fechaMod=fechaMod
    
    db.create_all()
    db.session.commit()