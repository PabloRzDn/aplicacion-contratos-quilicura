from flask import Blueprint, render_template, redirect, url_for, request, flash
from sqlalchemy.util.langhelpers import NoneType
from werkzeug.security import generate_password_hash, check_password_hash
from wtforms import validators
import datetime
from flask_login import login_required, current_user, login_user, logout_user
from app.admin.views import EliminarPerfil

from app.auth.models import  User, Contrato, Logs
from app.auth.forms import  RegistroUser, Login
from app.logs import LogsTareas
from app.actiondb import ModeloBaseDB
from app.indicadores import Indicadores
import sys

auth=Blueprint("auth",__name__,template_folder="templates")


@auth.route("/registrar")
@login_required
def RegistrarUser():
    return render_template("registro.html")


@auth.route("/formulario", methods=["GET","POST"])
@login_required
def FormularioUser():
    

     formulario_user=RegistroUser(request.form)
     if request.method=="POST":
         user=User(
             formulario_user.nombresUser.data,
             formulario_user.apellidosUser.data,
             formulario_user.rutUser.data,
             formulario_user.fechaNacimientoUser.data,
             formulario_user.emailUser.data,
             formulario_user.telefonoUser.data,
             formulario_user.cargoUser.data,
             generate_password_hash(formulario_user.contrasenaUser.data, method="sha256"),
             formulario_user.rolUser.data,
             str(datetime.datetime.now())
         )
         
         ModeloBaseDB.guardar(user)
         
         its_log="{} {}".format(current_user.nombresUser,current_user.apellidosUser)
         usuario_creado_log="{} {}".format(formulario_user.nombresUser.data,formulario_user.apellidosUser.data)
         tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S"))) 
         log_crear_user=LogsTareas(its_log,tiempo_creacion,usuario_creado_log)
         log_crear_user=log_crear_user.logCrearUsuario()

         log=Logs(current_user.id,
         datetime.datetime.now(),
         log_crear_user
         )

         ModeloBaseDB.guardar(log)
         
         flash(str("Usuario "+formulario_user.nombresUser.data+" creado satisfactoriamente"))
         
         return redirect(url_for("admin.Dashboard"))
         


@auth.route("/login")
def LoginRoute():
    return render_template("login.html")

@auth.route("/formulariologin", methods=["GET","POST"])
def Ingreso():
    
    formulario_login=Login(request.form)
    print('[+] Formulario Registrado', file=sys.stderr)

    if formulario_login.is_submitted():

        print('[+] Formulario Enviado correctamente', file=sys.stderr)
        #print('[+] Formulario Validado por validate_on_submit', file=sys.stderr)


        if request.method=="POST":
            
            print('[+] "POST"', file=sys.stderr)
            emailUser=formulario_login.emailUser.data
            contrasenaUser=formulario_login.contrasenaUser.data    
            user_login=ModeloBaseDB.seleccionarEmail(User,emailUser)
            print("[+] Usuario encontrado: ", user_login)

            if user_login==None:

                print("[!] Usuario No existe en la base de datos")
                flash("el usuario o contraseña no existe")
                return redirect(url_for("auth.LoginRoute"))


            if not emailUser or not check_password_hash(user_login.contrasenaUser,contrasenaUser):
                print('[!] No se reconoce email o no coincide chequeo con hash de contraseña', file=sys.stderr)
                flash("el usuario o contraseña no existe")
                return redirect(url_for("auth.LoginRoute"))

            login_user(user_login)
            print('[+] Usuario logueado', file=sys.stderr)
            if user_login.rolUser=="ADMINISTRADOR":
                print('[+] Usuario logueado como Administrador', file=sys.stderr)
            
                return redirect(url_for("admin.Dashboard"))
            elif user_login.rolUser=="USUARIO":
                print('[+] Usuario logueado como Usuario ITS', file=sys.stderr)
                return redirect(url_for("user.Dashboard"))
            else:
                redirect(url_for("auth.LoginRoute"))
        
        print('[!] Método "POST" no reconocido', file=sys.stderr)    


    else:
        print('[!] Formulario no validado por validate_on_submit', file=sys.stderr)
        flash("el usuario o contraseña no existe")
        return redirect(url_for("auth.LoginRoute"))



@auth.route("/loginits")
def Signup():
    return "Login ITS"

@auth.route("/logout")
def Logout():
    logout_user()
    return redirect(url_for("Index"))

