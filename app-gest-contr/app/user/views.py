from datetime import date, datetime
from flask import Blueprint, render_template, request, flash
from flask.helpers import url_for
from werkzeug.utils import redirect
from flask_login import login_required, current_user
import datetime
import sys


from app.actiondb import ModeloBaseDB
from app.auth.models import Contrato, Logs, User,Multas,Facturas
from app.indicadores import Indicadores
from app.user.forms import RegistroMulta, RegistroFactura
from app.upload import Archivos
from app.logs import LogsTareas
user=Blueprint("user",__name__,template_folder="templates")


@user.route("/dashboarduser", methods=["GET","POST"])
@login_required
def Dashboard():
    total_contratos=ModeloBaseDB.seleccionarItsId(Contrato,current_user.id)
    total_logs=ModeloBaseDB.seleccionarItsId(Logs,current_user.id)
    total_multas=ModeloBaseDB.seleccionarUserId(Multas,current_user.id)
    total_facturas=ModeloBaseDB.seleccionarUserId(Facturas,current_user.id)

    return render_template("usrseguircontratos.html",
    total_contratos=total_contratos,
    total_logs=total_logs,
    total_multas=total_multas,
    total_facturas=total_facturas)


@user.route("/usrcontrato/<id>",methods=["GET","POST"])
@login_required
def AccionesContrato(id):
    modelo_contrato=ModeloBaseDB.seleccionarId(Contrato,id)
    id_its=ModeloBaseDB.seleccionarId(Contrato,id).its_id
    its=ModeloBaseDB.seleccionarId(User,id_its)

    return render_template("usrcontrato.html", 
    its=its,
    modelo_contrato=modelo_contrato)



@user.route("/usrmulta/<id>", methods=["GET","POST"])
@login_required
def Multa(id):
    return render_template("multas.html",contrato_id=id)


@user.route("/registromulta", methods=["GET","POST"])
@login_required
def RegistrarMulta():
    formulario_multa=RegistroMulta(request.form)
    if request.method=="POST":

        registro_multas=Multas(
            user_id=formulario_multa.user_id.data,
            contrato_id=formulario_multa.contrato_id.data,
            glosa=formulario_multa.glosa.data,
            monto=formulario_multa.monto.data,
            statusMulta=formulario_multa.statusMulta.data
            
        )
        ModeloBaseDB.guardar(registro_multas)

        usuario_log="{} {}".format(current_user.nombresUser, current_user.apellidosUser)
        tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S")))        
        log_multa=LogsTareas(usuario_log,tiempo_creacion,None)
        log_multa=log_multa.logRegistrarMulta()
        print("[!] Multa guardada",file=sys.stderr)

        log=Logs(current_user.id,
        tiempo_creacion,
        log_multa
        )

        ModeloBaseDB.guardar(log)
        print("[!] Log almacenado",file=sys.stderr)

        flash("Multa registrada exitosamente")
    return redirect(url_for("user.Dashboard"))


@user.route("/usrfactura/<id>", methods=["GET","POST"])
@login_required
def Factura(id):
    return render_template("facturas.html", contrato_id=id)

@user.route("/registrarfactura", methods=["GET","POST"])
@login_required
def RegistrarFactura():
    formulario_factura=RegistroFactura(request.form)
    if request.method=="POST":
        registro_factura=Facturas(
            formulario_factura.user_id.data,
            formulario_factura.contrato_id.data,
            formulario_factura.docFactura.data,
            formulario_factura.statusFactura.data,
            str(datetime.datetime.now())
        )

        print(formulario_factura.user_id.data,
            formulario_factura.contrato_id.data)

        ModeloBaseDB.guardar(registro_factura)
        usuario_log="{} {}".format(current_user.nombresUser, current_user.apellidosUser)
        tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S")))        

        log_factura=LogsTareas(usuario_log,tiempo_creacion,formulario_factura.docFactura.data)
        log_factura=log_factura.logRegistrarFactura()

        log=Logs(current_user.id,
        tiempo_creacion,
        log_factura
        )

        ModeloBaseDB.guardar(log)
       

        
        flash("Factura Registrada exitosamente")
        return redirect(url_for("user.Dashboard"))


@user.route("/editarfactura/<id>", methods=["GET","POST"])
@login_required
def EditarFactura(id): 
    factura_a_editar=ModeloBaseDB.seleccionarId(Facturas,id)
    
    return render_template("editarfactura.html", factura_a_editar=factura_a_editar)


@user.route("/actualizarfactura/<id>", methods=["GET","POST"])
@login_required
def ActualizarFactura(id):
    formulario_edicion=RegistroFactura(request.form)
    if request.method=="POST":
        
        ModeloBaseDB.editarUsuario(Facturas,id,formulario_edicion)
        usuario_log="{} {}".format(current_user.nombresUser, current_user.apellidosUser)
        tiempo_creacion="{} {}".format(str(datetime.date.today()),str(datetime.datetime.now().time().strftime("%H:%M:%S")))        

        log_edit_factura=LogsTareas(usuario_log,tiempo_creacion,formulario_edicion.docFactura.data)
        log_edit_factura=log_edit_factura.logEditarFactura()
        print(log_edit_factura,file=sys.stderr)
        log=Logs(current_user.id,
        tiempo_creacion,
        log_edit_factura)
        ModeloBaseDB.guardar(log)

        flash("Factura editada Satisfactoriamente")
    return redirect(url_for("user.Dashboard"))


@user.route("/editarmulta/<string:id>", methods=["GET","POST"])
@login_required
def EditarMulta(id):
    multa_a_editar=ModeloBaseDB.seleccionarId(Multas,id)
    return render_template("editarmulta.html", multa_a_editar=multa_a_editar)


@user.route("/actualizarmulta/<id>", methods=["GET","POST"])
@login_required
def ActualizarMulta(id):
    formulario_edicion=RegistroMulta(request.form)
    if request.method =="POST":
        ModeloBaseDB.editarUsuario(Multas,id,formulario_edicion)
        flash("Multa editada satisfactoriamente")
        return redirect(url_for("user.Dashboard"))
