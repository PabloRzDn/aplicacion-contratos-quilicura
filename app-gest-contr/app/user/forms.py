from flask_wtf import FlaskForm, Form
from wtforms import SubmitField, DateField, HiddenField ,StringField, SelectField, PasswordField, DateTimeField, validators
from wtforms.validators import DataRequired, InputRequired, Email, Length

class RegistroMulta(Form):
   user_id=HiddenField("user_id",validators=[DataRequired()])
   contrato_id=HiddenField("contrato_id", validators=[DataRequired()])
   glosa=StringField("glosa",validators=[DataRequired()])
   monto=StringField("monto",validators=[DataRequired()])
   statusMulta=StringField("statusMulta",validators=[DataRequired()])


class RegistroFactura(Form):
   user_id=HiddenField("user_id",validators=[DataRequired()])
   contrato_id=HiddenField("contrato_id", validators=[DataRequired()])
   docFactura=StringField("docFactura", validators=[DataRequired()])
   statusFactura=StringField("statusFactura", validators=[DataRequired()])