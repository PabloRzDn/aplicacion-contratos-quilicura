from app import db
from sqlalchemy import desc

class ModeloBaseDB:
    def guardar(User):
        db.session.add(User)
        db.session.commit()

    @classmethod
    def contarCol(self,cls):
        self.contar=db.session.query(cls).count()
        return self.contar
    @classmethod
    def seleccionarId(self,cls,id):
        self.user=cls.query.filter_by(id=id).first()
        db.session.commit()
        return self.user
    @classmethod
    def seleccionarEmail(self,cls,emailUser):
        self.user=cls.query.filter_by(emailUser=emailUser).first()
        db.session.commit()
        return self.user
    @classmethod
    def seleccionarItsId(self,cls,id):
        self.user=db.session.query(cls).filter_by(its_id=id).order_by(cls.id.desc()).all()
        db.session.commit()
        return self.user
    @classmethod
    def seleccionarUserId(self,cls,id):
        self.user=cls.query.filter_by(user_id=id).all()
        db.session.commit()
        return self.user
    
    @classmethod
    def seleccionarTodosPorId(self,cls,id):
        self.contrato=cls.query.filter_by(id=id).all()
        db.session.commit()
        return self.contrato
    @classmethod
    def seleccionarTodosId(self,cls,id):
        self.user=cls.query.filter_by(id=id).all()
        db.session.commit()
        return self.user
    
    @classmethod
    def seleccionarUno(self,cls,busqueda):
        self.user=cls.query.filter_by(ofertaEconomica=busqueda).first()
        db.session.commit()
        return self.user
    
    @classmethod
    def seleccionarColumna(self,cls):
        self.col=db.session.query(cls.ofertaEconomica)
        db.session.commit()
        return self.col
    @classmethod
    def seleccionarColumnaFechas(self,cls):
        self.col=db.session.query(cls.fechaFinContrato)
        db.session.commit()
        return self.col
        
    @classmethod
    def seleccionarColumnaStatusFactura(self,cls):
        self.col=db.session.query(cls.statusFactura)
        db.session.commit()
        return self.col
    @classmethod
    def seleccionarColumnaStatusMulta(self,cls):
        self.col=db.session.query(cls.statusMulta)
        db.session.commit()
        return self.col
  
    
    @classmethod
    def eliminarUno(self,cls,id):
        cls.query.filter_by(id=id).delete()
        db.session.commit()
    
    @classmethod
    def seleccionarTodo(self,cls):
        return cls.query.all()

    @classmethod
    def seleccionarTodoDesc(self,cls):
        return db.session.query(cls).order_by(cls.id.desc()).limit(10).all()


    @classmethod
    def editarUsuario(self,cls,id,form):
        self.usuario_existente=cls.query.filter_by(id=id).first()
        form.populate_obj(self.usuario_existente)
        db.session.commit()
    

    @classmethod
    def seleccionarPorContratoId(self,cls,id):
        #self.factura=cls.query.filter_by(contrato_id=id).first()
        self.factura=db.session.query(cls).order_by(cls.id.desc()).filter_by(contrato_id=id).all()
        db.session.commit()
        return self.factura
        

    