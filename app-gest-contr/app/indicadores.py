import datetime
import re, sys
from flask_sqlalchemy.model import Model
import pygal
from pygal.style import LightStyle, Style

from app.auth.models import Facturas


from .actiondb import ModeloBaseDB


class Indicadores:
    def __init__(self,fecha_contrato,fecha_actual):
        self.fecha_contrato=fecha_contrato
        self.fecha_actual=fecha_actual
        
        
        self.contador_mas_de_120=0
        self.contador_mas_de_90=0
        self.contador_mas_de_30=0
        self.contador_menos_de_30=0
        self.contador_vencidos=0


        
    def tipoyValor(self):
        
        print(type(self.fecha_contrato))
        print(self.fecha_actual)
    
        #print(type(self.fecha_contrato))
        return self.fecha_contrato

    def estandarizar_tiempos(self,fecha_contr):
        if re.search("\.[\s\S]*$",fecha_contr):
            print("[+] Se inserta fecha con formato decimales en segundos", file=sys.stderr)
            
            self.fecha_contrato=datetime.datetime.strptime(fecha_contr.replace(re.search("\.[\s\S]*$",fecha_contr).group(),""),"%Y-%m-%d %H:%M:%S")
            return fecha_contr        
        else:
            #print("[+] Se inserta fecha en formato AAAA-MM-DD", file=sys.stderr)
            
            fecha_contr=datetime.datetime.strptime(fecha_contr,"%Y-%m-%d")
            return fecha_contr
        
    def vencimiento_contratos(self):
        
        if re.search("\.[\s\S]*$",self.fecha_contrato):
            print("[+] Se inserta fecha con formato decimales en segundos", file=sys.stderr)
            
            self.fecha_contrato=datetime.datetime.strptime(self.fecha_contrato.replace(re.search("\.[\s\S]*$",self.fecha_contrato).group(),""),"%Y-%m-%d %H:%M:%S")
        else:
            #print("[+] Se inserta fecha en formato AAAA-MM-DD", file=sys.stderr)
            
            self.fecha_contrato=datetime.datetime.strptime(self.fecha_contrato,"%Y-%m-%d")
        self.delta_tiempo=self.fecha_contrato-self.fecha_actual
        self.delta_tiempo=self.delta_tiempo.days
        return self.delta_tiempo

    @classmethod
    def obtener_contadores_facturas(self,cls):
        self.contador_fact_rech=0
        self.contador_fact_acept=0
        self.contador_fact_regis=0
        
        self.estado_objeto=ModeloBaseDB.seleccionarColumnaStatusFactura(cls)
        for i in range(self.estado_objeto.count()):
            if self.estado_objeto[i][0]=="RECHAZADA":
                 self.contador_fact_rech+=1
            elif self.estado_objeto[i][0]=="ACEPTADA":
                self.contador_fact_acept+=1
            elif self.estado_objeto[i][0]=="REGISTRADA":
                self.contador_fact_regis+=1
        return [self.contador_fact_regis,self.contador_fact_acept,self.contador_fact_rech] 

    @classmethod
    def obtener_contadores_multas(self,cls):

        self.contador_mult_curs=0
        self.contador_mult_notif=0
        self.contador_mult_impug=0
        self.contador_mult_apel=0
        self.contador_mult_ejec=0

        self.estado_objeto=ModeloBaseDB.seleccionarColumnaStatusMulta(cls)
        for i in range(self.estado_objeto.count()):
            if self.estado_objeto[i][0]=="CURSADA":
                self.contador_mult_curs+=1
            elif self.estado_objeto[i][0]=="NOTIFICADA":
                self.contador_mult_notif+=1
            elif self.estado_objeto[i][0]=="IMPUGNADA":
                self.contador_mult_impug+=1
            elif self.estado_objeto[i][0]=="APELADA":
                self.contador_mult_apel+=1
            elif self.estado_objeto[i][0]=="EJECUTORIADA":
                self.contador_mult_ejec+=1

        return [self.contador_mult_curs,self.contador_mult_notif,self.contador_mult_impug,self.contador_mult_apel,self.contador_mult_ejec]









class GraficoTorta:
    def __init__(self,lista_leyendas,lista_valores):
        self.lista_leyendas=lista_leyendas
        self.lista_valores=lista_valores
    
    def generar_grafico(self):
        self.grafico=pygal.Pie(inner_radius=.4, legend_at_bottom=True, style=Style(background="transparent"))
        for i in range(len(self.lista_valores)):
            self.grafico.add(self.lista_leyendas[i],self.lista_valores[i])
        self.data_grafico=self.grafico.render_data_uri()
        return self.data_grafico





