#importación de módulos externos
from flask import Flask
from flask.globals import current_app, session
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_login import LoginManager, login_user, logout_user, current_user
from functools import wraps

#se crea objeto Flask
app=Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"]="sqlite:///contratos_db.db"
#app.config["SQLALCHEMY_DATABASE_URI"]="mysql:pymysql//root@localhost/contratos_db"

#se inicializa instancia de bases de datos
db=SQLAlchemy(app)

ma=Marshmallow(app)
db.init_app(app)

login_manager=LoginManager()
login_manager.login_view="auth.LoginRoute"
login_manager.init_app(app)

from app.auth.models import User

#Decorador para recordar user conectado
@login_manager.user_loader
def load_user(user_id):
    return User.query.get((int(user_id)))

app.config.from_object("config.DevelopmentConfig")

#importación de variables de los blueprints
from app.admin.views import admin as admin_blueprint
from app.user.views import user as user_blueprint
from app.auth.views import auth as auth_blueprint

#registra los blueprints creados en los directorios user y admin
app.register_blueprint(admin_blueprint,url_prefix="/admin")
app.register_blueprint(user_blueprint,url_prefix="/user")
app.register_blueprint(auth_blueprint,url_prefix="/auth")

from . import views
