from werkzeug.security import generate_password_hash
import datetime
import sys

from app import app,db
from app.auth.models import User,Contrato, Archivo, Logs, Multas, Facturas
app.config["SECRET_KEY"]="secretkey"
app.config["SQLALCHEMY_DATABASE_URI"]="sqlite:///contratos_db.db"

def crear_tablas(User,Contrato, Archivo, Logs, Multas, Facturas):
    db.create_all()
    db.session.commit()
    print("[+] Tabla de bases de datos creadas")
    pass

def crear_administrador(User):
    admin=User(
        nombresUser="Administrador",
        apellidosUser="De Sitio",
        rutUser="18081516-3",
        fechaNacimientoUser=datetime.date(1991,7,30),
        emailUser="admin@admin",
        telefonoUser="123456789",
        cargoUser="Administrador de sitio",
        contrasenaUser=generate_password_hash("admin",method="sha256"),
        rolUser="ADMINISTRADOR",
        fechaMod=str(datetime.datetime.today())


    )
    db.session.add(admin)
    print("[+] Usuario administrador de sitio agregado", file=sys.stderr)
    db.session.commit()
    print("[+] Commit de usuario Realizado", file=sys.stderr)

    pass



if db.session.query(User).count()==0:
    print("[!] La tabla está vacia",file=sys.stderr)
    crear_tablas(User,Contrato, Archivo, Logs, Multas, Facturas)
    crear_administrador(User)

else:
    print("[!] Las tablas no están vacias",file=sys.stderr)
    pass








    

